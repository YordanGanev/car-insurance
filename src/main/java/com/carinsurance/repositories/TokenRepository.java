package com.carinsurance.repositories;

import com.carinsurance.models.VerificationToken;

public interface TokenRepository {

    VerificationToken create(VerificationToken token);

    void delete(VerificationToken token);

    VerificationToken getByToken(String token);
}
