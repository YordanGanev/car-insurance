package com.carinsurance.repositories.vehicles;

import com.carinsurance.models.vehicles.Type;

public interface TypeRepository {
    Type create(Type type);

    Type getByType(String type);

    void validateUniqueName(String type);

    boolean existsByType(String type);
}
