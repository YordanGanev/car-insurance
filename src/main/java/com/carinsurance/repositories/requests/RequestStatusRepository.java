package com.carinsurance.repositories.requests;

import com.carinsurance.models.requests.RequestStatus;

public interface RequestStatusRepository {
    RequestStatus getById(int id);

    RequestStatus getByStatus(String newStatus);
}
