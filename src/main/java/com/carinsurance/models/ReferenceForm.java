package com.carinsurance.models;

import com.carinsurance.models.requests.PremiumReference;
import lombok.Data;
import lombok.Setter;

import java.util.List;

@Data
@Setter
public class ReferenceForm {

    private List<PremiumReference> references;

}
