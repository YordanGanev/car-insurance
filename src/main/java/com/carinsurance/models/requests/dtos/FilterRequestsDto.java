package com.carinsurance.models.requests.dtos;

import lombok.Data;

@Data
public class FilterRequestsDto {

    public String user;
    public String requestDateMin;
    public String requestDateMax;
}
