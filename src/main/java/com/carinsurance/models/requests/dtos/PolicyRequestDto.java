package com.carinsurance.models.requests.dtos;

import lombok.Data;

@Data
public class PolicyRequestDto {

    private String effectiveDate;

    private String docUrl;

}
