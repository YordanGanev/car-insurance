package com.carinsurance.services.requests;


import com.carinsurance.models.requests.PremiumReference;
import com.carinsurance.models.requests.QuoteRequest;
import com.carinsurance.models.requests.dtos.RequestCreateDto;
import com.carinsurance.models.users.User;

import java.util.List;

public interface QuoteRequestService {

    QuoteRequest createQuoteRequest(RequestCreateDto requestCreateDto, User user);

    QuoteRequest getQuoteRequestById(int request_id, User author);

    List<QuoteRequest> getAllQuoteRequests(User userToShow, User userLogged);

    List<PremiumReference> getAllReferences();

    List<PremiumReference> updateReferences(List<PremiumReference> updatedReferences);

    double calculateTotalPremium(QuoteRequest quoteRequest);

}
