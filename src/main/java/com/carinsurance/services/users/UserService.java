package com.carinsurance.services.users;


import com.carinsurance.models.VerificationToken;
import com.carinsurance.models.users.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    void create(User user);

    void update(User updatedUser, User author);

    void delete(User user, User username);

    VerificationToken createVerificationToken(User user, String token);

    void deleteVerificationToken(VerificationToken verificationToken);

    VerificationToken getVerificationToken(String token);
}
