package com.carinsurance.validation.utils;

import com.carinsurance.models.requests.PolicyRequestDetails;

public interface DateComparator {

    int compare(PolicyRequestDetails p1, PolicyRequestDetails p2);
}
